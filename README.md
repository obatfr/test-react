# [Obat] Test technique React

Le projet est configuré avec :

-   eslint
-   prettier
-   typescript

## Requirements

-   Node.js

## Getting started

-   `npm install`
-   `npm start`

-   Lancer les tests : `npm test`
-   Check eslint : `npm run lint`
-   Typescript check : `npm run type-check`

## Exercice

L'exercice consiste à créer une liste de films (en utilisant l'api themoviedb) avec un système de pagination basique. Au clic sur un film on ouvrira la fiche du film en question avec des informations supplémentaire.

Quelques règle à suivre si possible :

-   Utilisation de typescript
-   Respect du linter
-   Test unitaire

### Page liste des films

La page liste des films devra être fournir les informations suivante pour chaque film :

-   Titre
-   Date de sortie
-   Note
-   Affiche du film

Vous pouvez vous inspirer de ce rendu :

![maquette](https://gitlab.com/obatfr/test-react/-/raw/main/maquette.jpg)

### Documentation API

-   Documentation API : https://developers.themoviedb.org/3/getting-started/introduction
-   Authentification : https://developers.themoviedb.org/3/getting-started/authentication
-   Liste des films : https://developers.themoviedb.org/3/discover/movie-discover

Base Image Url : `https://image.tmdb.org/t/p/original/`
